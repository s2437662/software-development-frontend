import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const CustomCard = (collectionTitle, collectionDescription, handleViewCollection, handleDelete) => {
  return (
    <Card sx={{ maxWidth: 250, margin: 2 }}>      
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          Lizard
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Lizards are a widespread group of squamate reptiles, with over 6,000
          species, ranging across all continents except Antarctica
        </Typography>
      </CardContent>
      <CardActions sx={{display: "flex", justifyContent: "center"}}>
        <Button size="small">View</Button>
        <Button size="small">Delete</Button>     
      </CardActions>
    </Card>
  );
};

export default CustomCard;
