import { useState } from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";

const CustomList = ({
  columnHeaders,
  columnBody,
  allowOnClick = false,
  handleOnClick = () => {}
}) => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <TableContainer>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columnHeaders.map((header) => (
                <TableCell
                  key={header.id}
                  align={header.align}
                  style={{ minWidth: header.minWidth }}
                  sx={{}}
                >
                  <b>{header.label}</b>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>

          <TableBody sx={{ cursor: "pointer" }}>
            {columnBody
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((data) => {
                return (
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={data.gameId}
                  >
                    {columnHeaders.map((header) => {
                      return header.id === "thumbnail" ? (
                        <TableCell
                          key={header.id}
                          onClick={
                            allowOnClick
                              ? () => {                                  
                                  handleOnClick(data.gameId)
                                }
                              : null
                          }
                        >
                          <img
                            src={data[header.id]}
                            alt={data["gameTitle"] + "-thumbnail"}
                          />
                        </TableCell>
                      ) : (
                        <TableCell
                          key={header.id}
                          onClick={
                            allowOnClick
                              ? () => {
                                  handleOnClick(data.gameId);
                                }
                              : null
                          }
                        >
                          {data[header.id]}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={columnBody.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default CustomList;
