import * as React from "react";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import CircularProgress from "@mui/material/CircularProgress";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
};

const CustomModalForm = ({
  openModal,
  setModalState,
  formTitle,
  buttonTitle,
  formField,
  isCreatingCollection,
  handleFormButtom = () => {},
  setFormValue = () => {},
}) => {
  return (
    <div>
      <Modal
        open={openModal}
        onClose={() => setModalState(false)}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={openModal}>
          <Box sx={style}>
            <Typography id="transition-modal-title" variant="h6" component="h2">
              {formTitle}
            </Typography>
            <Box mt={5} sx={{ display: "flex", flexDirection: "column" }}>
              {formField.map((field, id) => {
                return (
                  <Box mt={field.mt} key={id}>
                    <TextField
                      required
                      id={field.id}
                      name={field.id}
                      label={field.label}
                      fullWidth
                      onChange={(event) => {
                        let {value, name} = event.target
                        setFormValue((prevState) => ({
                          ...prevState,
                          [name]: value,
                        }));
                      }}
                    />
                  </Box>
                );
              })}
            </Box>
            <Box
              mt={5}
              sx={{ display: "flex", justifyContent: "space-evenly" }}
            >
              <Button
                variant="contained"
                color="error"
                size="small"
                onClick={() => {
                  setModalState(false);
                }}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                size="small"
                onClick={handleFormButtom}
              >
                {isCreatingCollection ? <CircularProgress /> : buttonTitle}
              </Button>
            </Box>
          </Box>
        </Fade>
      </Modal>
    </div>
  );
};

export default CustomModalForm;
