import Navbar from "./Navbar";
import { Outlet } from "react-router";

const Header = () => {
  return (
    <>
      <Navbar />
      <Outlet />
    </>
  )
}

export default Header