import React from "react";

import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";
import Fade from "@mui/material/Fade";

const MenuDropDown = ({
  menuItemList,
  openMenu,
  setOpenMenu,
  menuAnchorEl,
  setMenuEnchorEl,
  hanldeAddGameToMenu = () => {},
}) => {
  const handleClose = (event) => {
    setMenuEnchorEl(null);
    setOpenMenu(false);
  };

  return (
    <Menu
      id="fade-menu"
      MenuListProps={{
        "aria-labelledby": "fade-button",
      }}
      anchorEl={menuAnchorEl}
      open={openMenu}
      onClose={handleClose}
      TransitionComponent={Fade}
    >
      {menuItemList.length > 0 ? (
        menuItemList.map((item) => {
          return (
            <MenuItem onClick={hanldeAddGameToMenu} key={item.collection_id} id={item.collection_id}>
              {item.collection_title}
            </MenuItem>
          );
        })
      ) : (
        <MenuItem>No Collection Found</MenuItem>
      )}
    </Menu>
  );
};

export default MenuDropDown;
