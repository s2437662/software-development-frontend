import { useState } from "react";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router";
import { COLORS } from "../constants/constantColors";
import SearchBar from "./SearchBar";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import Tooltip from "@mui/material/Tooltip";
import MenuItem from "@mui/material/MenuItem";
import AdbIcon from "@mui/icons-material/Adb";

const PAGES = [
  {
    pageName: "Home",
    path: "/home",
    key: "home-page",
  },
  {
    pageName: "Recommendation",
    path: "/recommendation",
    key: "recommendation-page",
  },
  {
    pageName: "Community",
    path: "/community",
    key: "community-page",
  },
];

const SETTINGS = [
  {
    pageName: "Profile",
    path: "/profile",
    key: "profile-page",
  },
  {
    pageName: "Collection",
    path: "/collection",
    key: "collection-page",
  },
  {
    pageName: "Logout",
    path: "/login",
    key: "logout",
  },
];

const Navbar = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [cookie, setCookie, removeCookie] = useCookies("userToken");
  const navigate = useNavigate();

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  const handleLogout = () => {
    removeCookie("userToken");
    removeCookie("userId");
    navigate("/login");
  };

  return (
    <AppBar
      position="static"
      sx={{ bgcolor: COLORS.primaryColor, width: "100%" }}
    >
      <Container maxWidth="false">
        <Toolbar disableGutters>
          {/* mobile view burger menu */}
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {PAGES.map((item) => (
                <MenuItem
                  key={item.key}
                  onClick={() => {
                    handleCloseNavMenu();
                    navigate(item.path);
                  }}
                >
                  <Typography textAlign="center">{item.pageName}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          {/* mobile view logo */}
          <AdbIcon sx={{ display: { xs: "flex", md: "none" }, mr: 1 }} />
          <Typography
            variant="h6"
            noWrap
            component="a"
            onClick={() => navigate("/home")}
            sx={{
              mr: 1,
              display: { xs: "flex", md: "none" },
              flexGrow: 1,
              fontFamily: "monospace",
              fontWeight: 600,
              letterSpacing: ".1rem",
              color: "inherit",
              textDecoration: "none",
            }}
          >
            RABBIT GAMMING
          </Typography>

          {/* desktop view */}
          <AdbIcon sx={{ display: { xs: "none", md: "flex" }, mr: 1 }} />
          <Typography
            variant="h6"
            noWrap
            component="a"
            onClick={() => navigate("/home")}
            sx={{
              mr: 5,
              display: { xs: "none", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 600,
              letterSpacing: ".1rem",
              color: "inherit",
              textDecoration: "none",
              cursor: "pointer",
            }}
          >
            RABBIT GAMMING
          </Typography>
          <SearchBar />
          <Box
            sx={{
              flexGrow: 1,
              display: {
                xs: "none",
                md: "flex",
                justifyContent: "space-around",
              },
              mr: 2,
            }}
          >
            {PAGES.map((item) => (
              <Button
                key={item.key}
                sx={{ my: 2, color: "white", display: "block" }}
                onClick={() => navigate(item.path)}
              >
                {item.pageName}
              </Button>
            ))}
          </Box>

          <Box sx={{ flexGrow: 0 }}>
            <Tooltip title="Open settings">
              <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
              </IconButton>
            </Tooltip>
            <Menu
              sx={{ mt: "45px" }}
              id="menu-appbar"
              anchorEl={anchorElUser}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              {SETTINGS.map((item) => (
                <MenuItem
                  key={item.key}
                  onClick={() => {
                    handleCloseUserMenu();
                    navigate(item.path);
                    if (item.key === "logout") handleLogout();
                  }}
                >
                  <Typography textAlign="center">{item.pageName}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
export default Navbar;
