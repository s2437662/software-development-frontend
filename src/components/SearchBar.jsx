import { useState } from "react";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";

const SearchBar = () => {
  const [searchKeyword, setSearchKeyword] = useState();

  return (
    <Box
      sx={{
        flexGrow: 1,
        mr: 2,
        display: "flex",
        justifyContent: "center",
      }}
    >
      <TextField
        id="search-bar"
        className="text"
        onInput={(e) => {
          setSearchKeyword(e.target.value);
        }}
        label="Enter a board game name"
        variant="filled"
        placeholder="Search..."
        size="small"
        color="info"
        margin="none"
        fullWidth
        sx={{ bgcolor: "white", borderRadius: "5px" }}
      />
      <IconButton type="submit" aria-label="search">
        <SearchIcon style={{ fill: "white" }} />
      </IconButton>
    </Box>
  );
};

export default SearchBar;
