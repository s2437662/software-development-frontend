import React, { createContext, useState } from "react";
import {
  Route,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import { CookiesProvider } from "react-cookie";
import Home from "./Home";
import GameDetail from "./GameDetail";
import Profile from "./Profile";
import Header from "../components/Header";
import SignUp from "./SignUp";
import Login from "./Login";
import Collection from "./Collection";
import Veify from "./Verify";
import Community from "./Community";
import Recommentdation from "./Recommendation";
import CollectionDetail from "./CollectionDetail";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Header />}>
      <Route index element={<Login />} />
      <Route path="/login" element={<Login />} />
      <Route path="signup" element={<SignUp />} />
      <Route path="verify" element={<Veify />} />
      <Route path="home" element={<Home />} />
      <Route path="game-detail" element={<GameDetail />} />
      <Route path="profile" element={<Profile />} />                  
      <Route path="collection" element={<Collection />} />
      <Route path="collection-detail" element={<CollectionDetail />} />
      <Route path="recommendation" element={<Recommentdation />} />
      <Route path="community" element={<Community />} />
    </Route>
  )
);

export const Context = createContext();

const App = () => {
  const [appContext, setAppContext] = useState({
    gameId: "",
    userId: "",
    userEmail: "",
    collectionId: "",
  });

  return (
    <Context.Provider value={{ appContext, setAppContext }}>
      <CookiesProvider>
        <RouterProvider router={router} />
      </CookiesProvider>
    </Context.Provider>
  );
};

export default App;
