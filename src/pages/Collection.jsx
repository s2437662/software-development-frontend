import React, { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Box from "@mui/system/Box";
import CustomModalForm from "../components/CustomModalForm";
import useFetchData from "../utils/api/useFetchData";
import CircularProgress from "@mui/material/CircularProgress";
import { Context } from "./App";
import axios from "axios";
import Grid from "@mui/material/Grid";
import CustomSnackbar from "../components/CustomSnackbar";
import { useCookies } from "react-cookie";

const Collection = () => {
  const [openCollectionForm, setOpenCollectionForm] = useState(false);
  const [retrivingUserCollection, setRetrivingUserCollection] = useState(true);
  const { appContext, setAppContext } = useContext(Context);
  const [collectionFormData, setCollectionFormData] = useState({
    collectionTitle: "",
    description: "",
  });
  const [cookie] = useCookies("userToken");
  const [userIdCookie] = useCookies("userId");
  const FORM_FIELD = [
    { mt: 0, label: "Collection Title", id: "collectionTitle" },
    { mt: 2, label: "Description", id: "description" },
  ];
  const {
    isLoading: userCollectionIsLoading,
    apiData: userCollectionData,
    serverError: userCollectionError,
  } = useFetchData(
    "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/list-user-collection?account_id=" +
      userIdCookie.userId
  );
  const [snackbarState, setSnackbarState] = useState({
    open: false,
    message: null,
    severity: "success",
  });
  const [isCreatingCollection, setIsCreatingCollection] = useState(false);

  const navigate = useNavigate();

  const handleViewCollection = (collection_id) => {
    navigate("/collection-detail?collection_id=" + collection_id);
    setAppContext((prevState) => ({
      ...prevState,
      collectionId: collection_id,
    }));
  };

  const handleCreateCollection = async () => {
    setIsCreatingCollection(true);
    let requestBody = {
      body: {
        collection_title: collectionFormData.collectionTitle,
        description: collectionFormData.description,
        account_id: userIdCookie.userId,
      },
    };
    let response = await axios.post(
      "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/create-collection",
      requestBody
    );
    console.log(response);

    if (response.status === 200) {
      if (response.data.statusCode === 200) {
        setSnackbarState({
          open: true,
          message: response.data.body,
          severity: "success",
        });
        setTimeout(() => {
          setIsCreatingCollection(false);
          window.location.reload();
        }, 1000);
      } else {
        setSnackbarState({
          open: true,
          message: "Failed to create collection",
          severity: "error",
        });
      }
    } else {
      setSnackbarState({
        open: true,
        message: "Something unexpected happened, please try again later",
        severity: "error",
      });
    }
    setOpenCollectionForm(false);
  };

  useEffect(() => {
    if (cookie.userToken === undefined || cookie.userToken === "undefined") {
      navigate("/login");
    }
  }, []);

  useEffect(() => {
    setRetrivingUserCollection(true);
    if (userCollectionData != null) {
      console.log(userCollectionData);
      setRetrivingUserCollection(false);
    }
  }, [userCollectionIsLoading]);

  return (
    <>
      <Box>
        <CustomSnackbar
          snackbarOpen={snackbarState.open}
          setSnackbarOpen={setSnackbarState}
          severity={snackbarState.severity}
          message={snackbarState.message}
        />
        <Box sx={{ display: "flex", justifyContent: "flex-end", margin: 2 }}>
          <Button
            variant="contained"
            size="small"
            onClick={() => setOpenCollectionForm(true)}
          >
            Create New Collection
          </Button>
        </Box>
        {retrivingUserCollection === true ? (
          <Box
            sx={{
              display: "flex",
              height: "20rem",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <CircularProgress />
          </Box>
        ) : userCollectionData.length > 0 ? (
          <Box sx={{ display: "flex" }}>
            <Grid container mx={3} sx={{ justifyContent: "space-between" }}>
              {userCollectionData.map((collection) => {
                return (
                  <Card
                    sx={{ width: 250, margin: 2 }}
                    key={collection.collection_id}
                  >
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h5"
                        component="div"
                        sx={{
                          display: "-webkit-box",
                          overflow: "hidden",
                          WebkitBoxOrient: "vertical",
                          WebkitLineClamp: 1,
                        }}
                      >
                        {collection.collection_title}
                      </Typography>
                      <Typography
                        variant="body2"
                        color="text.secondary"
                        sx={{
                          height: 100,
                          display: "-webkit-box",
                          overflow: "hidden",
                          WebkitBoxOrient: "vertical",
                          WebkitLineClamp: 5,
                        }}
                      >
                        {collection.description}
                      </Typography>
                    </CardContent>
                    <CardActions
                      sx={{ display: "flex", justifyContent: "center" }}
                    >
                      <Button
                        size="small"
                        onClick={() =>
                          handleViewCollection(collection.collection_id)
                        }
                      >
                        View
                      </Button>
                      <Button size="small">Delete</Button>
                    </CardActions>
                  </Card>
                );
              })}
            </Grid>
          </Box>
        ) : (
          <Box
            sx={{
              display: "flex",
              height: "20rem",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Typography gutterBottom variant="h5" component="div">
              No Collection To Display
            </Typography>
          </Box>
        )}
      </Box>
      <CustomModalForm
        openModal={openCollectionForm}
        setModalState={setOpenCollectionForm}
        formTitle="Create New Collection"
        buttonTitle="Create Collection"
        formField={FORM_FIELD}
        setFormValue={setCollectionFormData}
        handleFormButtom={handleCreateCollection}
        isCreatingCollection={isCreatingCollection}
      />
    </>
  );
};

export default Collection;
