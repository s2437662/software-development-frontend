import React, { useState, useEffect, useContext } from "react";
import { Context } from "./App";
import { useNavigate } from "react-router";
import { useCookies } from "react-cookie";
import Box from "@mui/system/Box";
import CircularProgress from "@mui/material/CircularProgress";
import CustomList from "../components/CustomList";
import useFetchData from "../utils/api/useFetchData";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

const CollectionDetail = () => {
  const HEADERS = [
    { id: "thumbnail", label: "Game Image", minWidth: 100 },
    { id: "gameTitle", label: "Game Title", minWidth: 100 },
    {
      id: "gameDescription",
      label: "Game Description",
      minWidth: 200,
    },
  ];
  const { appContext, setAppContext } = useContext(Context);
  const [apiIsLoading, setApiIsLoading] = useState(true);
  const [gameData, setGameData] = useState(null);
  const [cookie] = useCookies("userToken");

  const { isLoading, apiData, serverError } = useFetchData(
    "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/get-collection-info?collection_id=" +
      appContext.collectionId
  );

  const navigate = useNavigate();
  
  useEffect(() => {
    if (cookie.userToken === undefined || cookie.userToken === "undefined") {
      navigate("/login");
    }
  }, []);

  useEffect(() => {
    setApiIsLoading(true);
    if (apiData != null) {
      setGameData(apiData.game_data_list);
      setApiIsLoading(false);
    }
  }, [isLoading]);

  const handleOnClick = (gameId) => {
    setAppContext((prevState) => ({ ...prevState, gameId: gameId }));
    navigate("/game-detail?game_id=" + gameId);
  };

  return (
    <Box sx={{ display: "block" }}>
      {apiIsLoading === true ? (
        <Box
          sx={{
            display: "flex",
            height: "20rem",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <div>
          <Grid item xl mx={2} my={2}>
            <Typography gutterBottom variant="h5">
              {apiData.collection_title}
            </Typography>
            <hr />
            <Typography variant="h6">Collection Description</Typography>
            <div>{apiData.description}</div>
            <hr style={{ marginTop: 15 }} />
            <Typography variant="h6">
              Total Number of Games In Collection: {gameData.length}
            </Typography>
          </Grid>
          {gameData.length > 0 ? (
            <CustomList
              columnHeaders={HEADERS}
              columnBody={gameData}
              allowOnClick={true}
              handleOnClick={handleOnClick}
            />
          ) : (
            <Box
              sx={{
                display: "flex",
                height: "20rem",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              There is no game in this collection!
            </Box>
          )}
        </div>
      )}
    </Box>
  );
};

export default CollectionDetail;
