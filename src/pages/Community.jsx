import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import Link from "@mui/material/Link";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";

// Generate Order Data
function createData(id, date, name, title, amount) {
  return { id, date, name, title, amount };
}

const rows = [
  createData(
    0,
    "12 Apr, 2023",
    "Elvis Presley",
    "How unforgiving is Food Chain Magnate really?",
    312
  ),
  createData(
    1,
    "12 Apr, 2023",
    "Paul McCartney",
    "Are there a lot of, and what are your favorite “build an engine with cards, power it with dice” games?",
    866
  ),
  createData(
    2,
    "12 Apr, 2023",
    "Tom Scholz",
    'Hidden Movement, but with majority of players as "runners"?',
    100
  ),
  createData(
    3,
    "12 Apr, 2023",
    "Michael Jackson",
    "Flatout Games announces Cascadia expansion Landmarks",
    654
  ),
  createData(
    4,
    "12 Apr, 2023",
    "Bruce Springsteen",
    "Where to find cheap drawstring dice/component bags?",
    212
  ),
  createData(
    5,
    "12 Apr, 2023",
    "Tsvetana Octavia",
    "Cards Against Humanity officially surpasses acoustic guitars as the most annoying thing you can bring to a party",
    882
  ),
  createData(
    6,
    "12 Apr, 2023",
    "Bea Pinchas",
    '"Are you gonna play the victim all life long?" My experience as a woman in board gaming, in brief.',
    472
  ),
  createData(
    7,
    "12 Apr, 2023",
    "Aonghus Olawale",
    "Guy Who Bitched for Five Straight Hours Wins Board Game at Last Second",
    272
  ),
  createData(
    8,
    "12 Apr, 2023",
    "Elfrida Karine",
    "After years of saving, I finally have my dream board game table with all the bells and whistles!",
    132
  ),
  createData(
    9,
    "12 Apr, 2023",
    "Kidist Shet",
    "My shelfie…. Just finished the build tonight!",
    522
  ),
];

function preventDefault(event) {
  event.preventDefault();
}

export default function Community() {
  const navigate = useNavigate();
  const [cookie] = useCookies("userToken");
  useEffect(() => {
    if (cookie.userToken === undefined || cookie.userToken === "undefined") {
      navigate("/login");
    }
  }, []);
  return (
    <React.Fragment>
      <Typography component="h1" variant="h5">
        Disscussion board
      </Typography>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Title</TableCell>
            <TableCell align="right">Views</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.title}</TableCell>
              <TableCell align="right">{`${row.amount}`}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
        See more forums
      </Link>
    </React.Fragment>
  );
}
