import { useState, useContext, useEffect } from "react";
import axios from "axios";
import { Context } from "./App";
import useFetchData from "../utils/api/useFetchData";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import Box from "@mui/system/Box";
import CircularProgress from "@mui/material/CircularProgress";
import Button from "@mui/material/Button";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import CustomList from "../components/CustomList";
import MenuDropDown from "../components/MenuDropDown";
import CustomSnackbar from "../components/CustomSnackbar";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";

const GameDetail = () => {
  const { appContext, setAppContext } = useContext(Context);
  const [apiIsLoading, setApiIsLoading] = useState(true);
  const [retrivingUserCollection, setRetrivingUserCollection] = useState(true);
  const [openMenu, setOpenMenu] = useState(false);
  const [menuAnchorEl, setMenuAnchorEl] = useState(null);
  const [tabValue, setTabValue] = useState("overview");
  const [cookie, setCookie] = useCookies("userToken");
  const [userIdCookie] = useCookies("userId");
  const [snackbarState, setSnackbarState] = useState({
    open: false,
    message: null,
    severity: "success",
  });
  const {
    isLoading: gameDataIsLoading,
    apiData: gameData,
    serverError: gameDataError,
  } = useFetchData(
    "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/get-game?game_id=" +
      appContext.gameId
  );

  const {
    isLoading: userCollectionIsLoading,
    apiData: userCollectionData,
    serverError: userCollectionError,
  } = useFetchData(
    "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/list-user-collection?account_id=" +
      userIdCookie.userId
  );

  const navigate = useNavigate();

  const hanldeAddGameToMenu = async (event) => {
    let requestBody = {
      body: {
        game_id: appContext.gameId,
        collection_id: event.target.id,
      },
    };
    let response = await axios.post(
      "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/add-to-collection",
      requestBody
    );

    if (response.status === 200) {
      if (response.data.statusCode === 200) {
        setSnackbarState({
          open: true,
          message: response.data.body,
          severity: "success",
        });
      } else if (response.data.statusCode === 400) {
        setSnackbarState({
          open: true,
          message: response.data.body,
          severity: "error",
        });
      }
    } else {
      setSnackbarState({
        open: true,
        message: "Something unexpected happened, please try again later",
        severity: "error",
      });
    }
    setOpenMenu(false);
  };

  const handleChangeTab = (event, newValue) => {
    setTabValue(newValue);
  };

  useEffect(() => {
    if (cookie.userToken === undefined || cookie.userToken === "undefined") {
      navigate("/login");
    }
  }, []);

  useEffect(() => {
    setApiIsLoading(true);
    if (gameData != null) {
      setApiIsLoading(false);
    }
  }, [gameDataIsLoading]);

  useEffect(() => {
    setRetrivingUserCollection(true);
    if (userCollectionData != null) {
      setRetrivingUserCollection(false);
    }
  }, [userCollectionIsLoading]);

  const Overview = () => {
    return (
      <Box>
        <Typography gutterBottom variant="h5">
          Description
        </Typography>
        <Typography gutterBottom>{gameData.game_description}</Typography>
      </Box>
    );
  };

  const Reviews = () => {
    const HEADERS = [
      { id: "author", label: "User", minWidth: 100 },
      { id: "review", label: "Reviews/Comments", minWidth: 200 },

      {
        id: "rating",
        label: "Rating",
        minWidth: 100,
      },
    ];

    return (
      <CustomList
        columnHeaders={HEADERS}
        columnBody={gameData.reviews_and_ratings}
        allowOnClick={false}
      />
    );
  };

  const Forum = () => {
    return <Box>Coming Soon!</Box>;
  };

  const Faq = () => {
    return <Box>Coming Soon!</Box>;
  };

  const renderTab = () => {
    switch (tabValue) {
      case "overview":
        return <Overview />;
      case "reviews":
        return <Reviews />;
      case "forum":
        return <Forum />;
      case "faq":
        return <Faq />;
      default:
        return <Overview />;
    }
  };

  return apiIsLoading === true ? (
    <Box
      sx={{
        display: "flex",
        height: "20rem",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <CircularProgress />
    </Box>
  ) : (
    <div>
      <Box sx={{ p: 2 }}>
        <Grid container spacing={2}>
          <CustomSnackbar
            snackbarOpen={snackbarState.open}
            setSnackbarOpen={setSnackbarState}
            severity={snackbarState.severity}
            message={snackbarState.message}
          />
          <Grid item>
            <Box>
              <img
                alt="complex"
                src={gameData.game_image}
                style={{ width: 300, height: 300 }}
              />
            </Box>
          </Grid>
          <Grid item xl={12} sm container>
            <Grid item xl container direction="column" spacing={2}>
              <Grid item xl>
                <Typography gutterBottom variant="h3">
                  {gameData.game_title}
                </Typography>
                <Typography variant="subtitle1">
                  Publication: {gameData.year_published}
                </Typography>
                <Typography variant="subtitle1">
                  Players: {gameData.min_players} - {gameData.max_players}
                </Typography>
                <Typography variant="subtitle1" component="div">
                  Ratings: {gameData.average_ratings.substring(0, 3)}
                </Typography>
              </Grid>

              <Grid
                item
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "left",
                }}
                mt={2}
              >
                <Box mb={1}>
                  <Button
                    variant="contained"
                    size="small"
                    onClick={(event) => {
                      setOpenMenu(true);
                      setMenuAnchorEl(event.currentTarget);
                    }}
                  >
                    {retrivingUserCollection
                      ? "Loading..."
                      : "Add To Collection"}
                  </Button>
                  <MenuDropDown
                    menuItemList={userCollectionData}
                    openMenu={openMenu}
                    setOpenMenu={setOpenMenu}
                    menuAnchorEl={menuAnchorEl}
                    setMenuEnchorEl={setMenuAnchorEl}
                    hanldeAddGameToMenu={hanldeAddGameToMenu}
                  />
                </Box>
                <Box my={1}>
                  <Button variant="contained" size="small">
                    Add Review
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <hr />
        <Box sx={{ width: "100%" }}>
          <Tabs
            value={tabValue}
            onChange={handleChangeTab}
            textColor="primary"
            indicatorColor="primary"
          >
            <Tab value="overview" label="Overview" />
            <Tab value="reviews" label="Reviews" />
            <Tab value="forum" label="Forum Discussion" />
            <Tab value="faq" label="FAQs" />
          </Tabs>
        </Box>
        <Box mt={2}>{renderTab()}</Box>
      </Box>
    </div>
  );
};

export default GameDetail;
