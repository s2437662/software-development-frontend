import React, { useEffect, useState, useContext } from "react";
import CustomList from "../components/CustomList";
import useFetchData from "../utils/api/useFetchData";
import { transformGameListData } from "../utils/utils";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import { useNavigate } from "react-router-dom";
import { Context } from "./App";
import { useCookies } from "react-cookie";

const Home = () => {
  const { isLoading, apiData, serverError } = useFetchData(
    "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/list-game"
  );
  const [gameData, setGameData] = useState([]);
  const [apiIsLoading, setApiIsLoading] = useState(true);
  const { appContext, setAppContext } = useContext(Context);
  const [cookie, setCookie] = useCookies("userToken");

  const navigate = useNavigate();

  const HEADERS = [
    { id: "thumbnail", label: "Game Image", minWidth: 100 },
    { id: "gameTitle", label: "Game Title", minWidth: 100 },
    {
      id: "gameDescription",
      label: "Game Description",
      minWidth: 200,
    },
    {
      id: "rating",
      label: "Rating",
      minWidth: 170,
    },
    {
      id: "playerNum",
      label: "Players",
      minWidth: 170,
    },
  ];

  const handleOnClick = (gameId) => {
    setAppContext((prevState) => ({ ...prevState, gameId: gameId }));
    navigate("/game-detail?game_id=" + gameId);
  };

  useEffect(() => {
    if (cookie.userToken === undefined || cookie.userToken === "undefined") {
      navigate("/login");
    }
  }, []);

  useEffect(() => {
    setApiIsLoading(true);
    if (apiData != null) {
      setGameData(transformGameListData(apiData));
      setApiIsLoading(false);
    }
  }, [isLoading]);

  return (
    <Box sx={{ display: "block" }}>
      {apiIsLoading === true ? (
        <Box
          sx={{
            display: "flex",
            height: "20rem",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <CircularProgress />
        </Box>
      ) : (
        <div>
          <CustomList
            columnHeaders={HEADERS}
            columnBody={gameData}
            allowOnClick={true}
            handleOnClick={handleOnClick}
          />
        </div>
      )}
    </Box>
  );
};

export default Home;
