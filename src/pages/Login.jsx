import { useState } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import CustomSnackbar from "../components/CustomSnackbar";
import CircularProgress from "@mui/material/CircularProgress";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router";
import { loginFormValidationSchema } from "../utils/yupValidation";
import { useCookies } from "react-cookie";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © A Website for your collection "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme();

export default function SignIn() {
  const [loginFormState, setLoginFormState] = useState({
    email: "",
    password: "",
  });
  const [snackbarState, setSnackbarState] = useState({
    open: false,
    message: null,
    severity: "success",
  });
  const [processingLogin, setProccessingLogin] = useState(false);

  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setLoginFormState({
      ...loginFormState,
      [name]: value,
    });
  };

  const [cookie, setCookie] = useCookies(["userToken", "userId"]);

  const loginUser = async (formData) => {
    let requestBody = {
      body: {
        ...formData,
      },
    };
    let response = await axios.post(
      "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/sign-in",
      requestBody
    );

    if (response.status === 200) {
      if (response.data.statusCode === 200) {
        let accessToken =
          response.data.body.auth_token.AuthenticationResult.AccessToken;
        let expireTime =
          response.data.body.auth_token.AuthenticationResult.ExpiresIn;

        setCookie("userToken", accessToken, { maxAge: 24 * expireTime });
        setCookie("userId", response.data.body.account_id, {
          maxAge: 24 * expireTime,
        });
        setSnackbarState({
          open: true,
          message: "Login Successful!",
          severity: "success",
        });
        setTimeout(() => {
          navigate("/home");
        }, 1000);
      } else {
        setSnackbarState({
          open: true,
          message: "Invalid email or password!",
          severity: "error",
        });
        setProccessingLogin(false);
      }
    } else {
      setSnackbarState({
        open: true,
        message: "Something unexpected happened, please try again later",
        severity: "error",
      });
      setProccessingLogin(false);
    }
  };

  const validateLoginForm = () => {
    loginFormValidationSchema
      .validate(loginFormState, { abortEarly: false })
      .then((value) => {
        loginUser(value);
        return value;
      })
      .catch((error) => {
        if (error.inner.length > 0) {
          setSnackbarState({
            open: true,
            message: error.inner[0].message,
            severity: "error",
          });
        }
        setProccessingLogin(false);
      });
  };

  const handleSubmit = (event) => {
    setProccessingLogin(true);
    event.preventDefault();
    validateLoginForm();
  };

  const navigate = useNavigate();

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <CustomSnackbar
          snackbarOpen={snackbarState.open}
          setSnackbarOpen={setSnackbarState}
          severity={snackbarState.severity}
          message={snackbarState.message}
        />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Login
          </Typography>
          <Box component="form" noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={handleFieldChange}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={handleFieldChange}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              color="inherit"
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleSubmit}
            >
              {processingLogin ? <CircularProgress /> : "Sign In"}
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2" sx={{ cursor: "pointer" }}>
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link
                  onClick={() => navigate("/signup")}
                  variant="body2"
                  sx={{ cursor: "pointer" }}
                >
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 8, mb: 4 }} />
      </Container>
    </ThemeProvider>
  );
}
