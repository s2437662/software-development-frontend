import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useCookies } from "react-cookie";
import Link from "@mui/material/Link";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";

// Generate Order Data
function createData(id, date, name, title, amount) {
  return { id, date, name, title, amount };
}

const rows = [
  createData(
    0,
    "Age of Innovation",
    "1",
    "Fantasy, Terra Mystica, Helge Ostertag",
    1312
  ),
  createData(
    1,
    " Earth (2023)",
    "2",
    "Strategy, Animals, Card Game, Environmental",
    866
  ),
  createData(
    2,
    "Ierusalem: Anno Domini",
    "3",
    "Religious, Mechanism, Solo / Solitaire Game",
    700
  ),
  createData(
    3,
    "Evacuation",
    "4",
    "Industry / Manufacturing, Racing, Science Fiction, Territory Building",
    654
  ),
  createData(
    4,
    "Hegemony: Lead Your Class to Victory",
    "5",
    "Strategy, Economic, Educational, Political",
    627
  ),
];

function preventDefault(event) {
  event.preventDefault();
}

export default function Recommendation() {
  const navigate = useNavigate();
  const [cookie] = useCookies("userToken");
  useEffect(() => {
    if (cookie.userToken === undefined || cookie.userToken === "undefined") {
      navigate("/login");
    }
  }, []);
  return (
    <React.Fragment>
      <Typography component="h1" variant="h5">
        Popular game of the week
      </Typography>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Game</TableCell>
            <TableCell>Rank</TableCell>
            <TableCell>Tags</TableCell>
            <TableCell align="right">Hotness</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.title}</TableCell>
              <TableCell align="right">{`${row.amount}`}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Link color="primary" href="#" onClick={preventDefault} sx={{ mt: 3 }}>
        See more forums
      </Link>
    </React.Fragment>
  );
}
