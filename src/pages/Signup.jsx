import { useState, useContext } from "react";
import axios from "axios";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import CircularProgress from "@mui/material/CircularProgress";
import CustomSnackbar from "../components/CustomSnackbar";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router";
import { signUpFormValidationSchema } from "../utils/yupValidation";
import { Context } from "./App";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © A Website for your collection "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme();

const SignUp = () => {
  const navigate = useNavigate();
  const [snackbarState, setSnackbarState] = useState({
    open: false,
    message: null,
    severity: "success",
  });
  const [formState, setFormState] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    confirmPassword: "",
    region: "",
  });
  const [processingSignUp, setProcessingSignUp] = useState(false);
  const { appContext, setAppContext } = useContext(Context);

  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    setFormState({
      ...formState,
      [name]: value,
    });
  };

  const signUpUser = async (formData) => {
    let requestBody = {
      body: {
        first_name: formData.firstName,
        last_name: formData.lastName,
        email: formData.email,
        password: formData.password,
        region: formData.region,
      },
    };

    let response = await axios.post(
      "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/sign-up",
      requestBody
    );

    if (response.status === 200) {
      if (response.data.statusCode === 200) {
        setSnackbarState({
          open: true,
          message: response.data.body.message,
          severity: "success",
        });
        setAppContext(() => ({
          ...appContext,
          userEmail: formData.email,
          userId: response.data.body.account_id,
        }));
        setTimeout(() => {
          navigate("/verify");
        }, 1000);
      } else {
        setSnackbarState({
          open: true,
          message: "Email already exists",
          severity: "error",
        });
        setProcessingSignUp(false);
      }
    } else {
      setSnackbarState({
        open: true,
        message: "Something unexpected happened, please try again later",
        severity: "error",
      });
      setProcessingSignUp(false);
    }
  };

  const validateSignUpForm = () => {
    signUpFormValidationSchema
      .validate(formState, { abortEarly: false })
      .then((value) => {
        signUpUser(value);
        return value;
      })
      .catch((error) => {
        if (error.inner.length > 0) {
          setSnackbarState({
            open: true,
            message: error.inner[0].message,
            severity: "error",
          });
        }
        setProcessingSignUp(false);
      });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setProcessingSignUp(true);
    validateSignUpForm();
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <CustomSnackbar
          snackbarOpen={snackbarState.open}
          setSnackbarOpen={setSnackbarState}
          severity={snackbarState.severity}
          message={snackbarState.message}
        />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <Box component="form" noValidate sx={{ mt: 3 }}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="given-name"
                  name="firstName"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="family-name"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  id="email"
                  label="Email Address"
                  name="email"
                  autoComplete="email"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="confirmPassword"
                  label="Confirm Password"
                  type="password"
                  id="confirm password"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  fullWidth
                  name="region"
                  label="Region"
                  type="region"
                  id="region"
                  autoComplete="region"
                  onChange={handleFieldChange}
                />
              </Grid>

              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox value="allowExtraEmails" color="primary" />
                  }
                  label="Agree with terms and conditions."
                />
              </Grid>
            </Grid>
            <Button
              color="inherit"
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
              onClick={handleSubmit}
            >
              {processingSignUp ? <CircularProgress /> : "Sign Up"}
            </Button>
            <Grid container justifyContent="flex-end">
              <Grid item>
                <Link
                  onClick={() => navigate("/login")}
                  variant="body2"
                  sx={{ cursor: "pointer" }}
                >
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
};

export default SignUp;
