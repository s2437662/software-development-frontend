import { useState, useContext } from "react";
import Button from "@mui/material/Button";
import axios from "axios";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import CustomSnackbar from "../components/CustomSnackbar";
import CircularProgress from "@mui/material/CircularProgress";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useNavigate } from "react-router";
import { Context } from "./App";

function Copyright(props) {
  return (
    <Typography
      variant="body2"
      color="text.secondary"
      align="center"
      {...props}
    >
      {"Copyright © A Website for your collection "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const theme = createTheme();
const Veify = () => {
  const [snackbarState, setSnackbarState] = useState({
    open: false,
    message: null,
    severity: "success",
  });
  const [verficationCode, setVerficationCode] = useState({
    1: "",
    2: "",
    3: "",
    4: "",
    5: "",
    6: "",
  });
  const [processingRequest, setProcessingRequest] = useState(false);
  const { appContext } = useContext(Context);

  const handleFieldChange = (e) => {
    const { name, value } = e.target;
    const REGEX = /[^0-9]/;
    if (REGEX.test(value)) {
      setVerficationCode({
        ...verficationCode,
        [name]: "",
      });
    } else {
      setVerficationCode({
        ...verficationCode,
        [name]: value,
      });
    }
  };

  const handleResendCode = () => {

  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setProcessingRequest(true);
    let {
      1: one,
      2: two,
      3: three,
      4: four,
      5: five,
      6: six,
    } = verficationCode;

    let userCodeInput = one + two + three + four + five + six;
    if (userCodeInput.length < 6) {
      setSnackbarState({
        open: true,
        message: "Please provide a valid verification code",
        severity: "error",
      });
    } else {
      let requestBody = {
        body: {
          verification_code: userCodeInput,
          email: appContext.userEmail,
        },
      };
      let response = await axios.post(
        "https://k97cng3b15.execute-api.eu-west-2.amazonaws.com/prod/verify-email",
        requestBody
      );

      if (response.status === 200) {
        if (response.data.statusCode === 200) {
          setSnackbarState({
            open: true,
            message: "Account Verified!",
            severity: "success",
          });
          setTimeout(() => {
            navigate("/login");
          }, 1000);
        } else {
          setSnackbarState({
            open: true,
            message: "Invalid Code!",
            severity: "error",
          });
          setProcessingRequest(false);
        }
      } else {
        setSnackbarState({
          open: true,
          message: "Something unexpected happened, please try again later",
          severity: "error",
        });
        setProcessingRequest(false);
      }
    }
  };

  const navigate = useNavigate();

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <CustomSnackbar
          snackbarOpen={snackbarState.open}
          setSnackbarOpen={setSnackbarState}
          severity={snackbarState.severity}
          message={snackbarState.message}
        />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Typography component="h1" variant="h5">
            Verification
          </Typography>
          <Box component="form" noValidate sx={{ mt: 3 }}>
            <Grid container spacing={1}>
              <Grid item xs={6} sm={2}>
                <TextField
                  required
                  fullWidth
                  autoFocus
                  inputProps={{ maxLength: 1 }}
                  name="1"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={6} sm={2}>
                <TextField
                  required
                  fullWidth
                  inputProps={{ maxLength: 1 }}
                  name="2"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={6} sm={2}>
                <TextField
                  required
                  fullWidth
                  inputProps={{ maxLength: 1 }}
                  name="3"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={6} sm={2}>
                <TextField
                  required
                  fullWidth
                  inputProps={{ maxLength: 1 }}
                  name="4"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={6} sm={2}>
                <TextField
                  required
                  fullWidth
                  inputProps={{ maxLength: 1 }}
                  name="5"
                  onChange={handleFieldChange}
                />
              </Grid>
              <Grid item xs={6} sm={2}>
                <TextField
                  required
                  fullWidth
                  inputProps={{ maxLength: 1 }}
                  name="6"
                  onChange={handleFieldChange}
                />
              </Grid>
            </Grid>
            <Button
              color="primary"
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3 }}
              onClick={handleSubmit}
            >
              {processingRequest ? <CircularProgress /> : "Verify"}
            </Button>
            <Button
              color="inherit"
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 1, mb: 2 }}
              onClick={handleResendCode}
            >
              Resend Code
            </Button>
            <Grid container justifyContent="flex-end"></Grid>
          </Box>
        </Box>
        <Copyright sx={{ mt: 5 }} />
      </Container>
    </ThemeProvider>
  );
};

export default Veify;
