import { useState, useEffect } from "react";
import axios from "axios";

const useFetchData = (apiEndpoint) => {
  const [isLoading, setIsLoading] = useState(false);
  const [apiData, setApiData] = useState(null);
  const [serverError, setServerError] = useState(null);

  const fetchData = async () => {
    try {
      setIsLoading(true);
      const response = await axios.get(apiEndpoint);
      const data = await response?.data;      
      setApiData(data);
      setIsLoading(false);
    } catch (error) {
      setServerError(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    fetchData();
  }, [apiEndpoint]);
  
  return { isLoading, apiData, serverError };
};

export default useFetchData;
