export const transformGameListData = (apiData) => {
  let gameData = apiData.map((game) => {
    return {
      thumbnail: game.thumbnail,
      gameTitle: game.game_title,
      gameDescription: game.game_description.substring(0, 500) + "...",
      rating: game.average_ratings.substring(0,3),
      playerNum: game.min_players + "-" + game.max_players,
      gameId: game.game_id,
    };
  });

  return gameData;
};
